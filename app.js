var fs = require('fs'),
    path = require('path'),
    prompt = require('prompt');

(function takeUserInput() {
    prompt.start();
    prompt.get('Enter_valid_url_path', function (err, result) {

        fs.exists(result.Enter_valid_url_path, (exists) => {
            if (exists) {
                crawl(result.Enter_valid_url_path);
            } //close if
            else {
                console.log("------INVALID URL------ENTER NEW URL---------------");
                takeUserInput();
            } //close else
        }); //close fs.exists

    });
})();



function crawl(dir) {
    console.log('[+]', dir);
    var files = fs.readdirSync(dir);
    for (var x in files) {
        var next = path.join(dir, files[x]);
        if (fs.lstatSync(next).isDirectory() == true) {
            crawl(next);

        } //closing if
        else {
            console.log('\t', next);
        }

    } //closing for
}



// crawl("/home/pradeep_shrestha/Desktop/amnilProjects/pop");